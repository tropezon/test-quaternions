﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTest : MonoBehaviour {

	public Transform objectToFollow;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 vectorToFollowObject =  objectToFollow.position - transform.position;
		vectorToFollowObject.Normalize ();

		//Debug.Log (vectorToFollowObject);
		Debug.Log ("Up vector" + vectorToFollowObject.ToString());
		/*

		x y z

		  
		 */

		float diffAngle = Vector3.Angle (vectorToFollowObject,transform.up)*Mathf.Deg2Rad;
		Debug.Log (diffAngle);
		float vectorLength = vectorToFollowObject.magnitude;

		//float xAngle = Mathf.Acos (vectorToFollowObject.x/vectorLength);
		//float yAngle = Mathf.Acos (vectorToFollowObject.y/vectorLength);
		//float zAngle = Mathf.Acos (vectorToFollowObject.z/vectorLength);

		Vector3 fordward = transform.forward;

		Vector3 newFordwardVector = fordward;
		newFordwardVector = Quaternion.Euler (diffAngle*Mathf.Rad2Deg,diffAngle*Mathf.Rad2Deg,diffAngle*Mathf.Rad2Deg)*fordward;
		/*
		Vector3 newFordwardVector = new Vector3 (
			fordward.x*Mathf.Cos(diffAngle),
			fordward.y*Mathf.Cos(diffAngle),
			fordward.z*Mathf.Cos(diffAngle)
		);
		*/
		Debug.Log ("Forward vector" + newFordwardVector.ToString());
		Quaternion newRotation = new Quaternion ();
		newRotation.SetLookRotation (newFordwardVector,vectorToFollowObject);

		transform.localRotation = newRotation;
	}
}
