﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ClassA : MonoBehaviour {

	public GameObject PrefabB;
	ClassB classB;

	void Awake(){
		classB = PrefabB.GetComponent<ClassB> ();
		classB.classC.TestEvent += new TestEventHandler (OnEventOfClassA);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnEventOfClassA(object sender, EventArgs e){
		Debug.Log ("Recieved event from Class A");
	}
}
