﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public delegate void TestEventHandler(object sender, EventArgs e); 

public class ClassC : MonoBehaviour {

	public event TestEventHandler TestEvent;

	public bool LaunchEvent = false;

	void Start(){
		TestEvent += new TestEventHandler (OnEventOfClassC);
	}

	void Update(){
		if (LaunchEvent) {
			TestEvent (this,EventArgs.Empty);
			LaunchEvent = false;
		}
	}

	void OnEventOfClassC(object sender, EventArgs e){
		Debug.Log ("Recieved event from Class C");
	}

}
